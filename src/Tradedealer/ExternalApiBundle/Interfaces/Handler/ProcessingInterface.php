<?php

namespace Tradedealer\ExternalApiBundle\Interfaces\Handler;

interface ProcessingInterface
{
    public function processing($data);
}
