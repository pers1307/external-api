<?php

namespace Tradedealer\ExternalApiBundle\Interfaces\Handler;

interface TransformInterface
{
    public function transform($data);
}
