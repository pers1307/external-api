<?php

namespace Tradedealer\ExternalApiBundle\Interfaces\Handler;

interface ValidateInterface
{
    public function validate($data);
}
