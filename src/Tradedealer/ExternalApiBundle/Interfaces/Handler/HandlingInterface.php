<?php

namespace Tradedealer\ExternalApiBundle\Interfaces\Handler;

interface HandlingInterface
{
    public function handling($data);
}
