<?php

namespace Tradedealer\ExternalApiBundle\Interfaces\Factory;

use Tradedealer\EntityBundle\Entity\UserApi;

interface CustomerOrderInterface
{
    public function getCustomerOrderInsert(UserApi $userApi);

    public function getCustomerOrderUpdate(UserApi $userApi);

    public function getCustomerOrderDelete(UserApi $userApi);
}
