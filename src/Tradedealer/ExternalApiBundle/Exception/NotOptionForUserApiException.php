<?php

namespace Tradedealer\ExternalApiBundle\Exception;

use Throwable;

class NotOptionForUserApiException extends \RuntimeException
{
    public function __construct($message, array $params = [], Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}