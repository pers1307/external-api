<?php

namespace Tradedealer\ExternalApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Tradedealer\EntityBundle\Entity\UserApi;
use Tradedealer\ExternalApiBundle\Handler\AbstractHandler\AbstractHandler;
use Tradedealer\ExternalApiBundle\HandlerFactory\HandlerFactory;

/**
 * @Rest\Prefix("order")
 * @Rest\NamePrefix("order-")
 *
 * Class CustomerOrderController
 * @package Tradedealer\ExternalApiBundle\Controller
 */
class CustomerOrderController extends FOSRestController
{
    /**
     * @var object|\Tradedealer\ExternalApiBundle\Shell\FactoryShell
     */
    protected $factoryShell;

    /**
     * @Rest\Post("")
     * @Rest\View(serializerGroups={"external"})
     *
     * @param ParamFetcher $paramFetcher
     * @return mixed
     */
    public function insertAction (ParamFetcher $paramFetcher)
    {
        $this->factoryShell = $this->get("tradedealer.factory_shell");

        /** @var UserApi $userApi */
        $userApi = $this->getUser();

        $request = [];

        $result = $this->factoryShell->handling($userApi, $request, 'getCustomerOrderInsert');

        return $result;
    }

    /**
     * @Rest\Patch("/{phone}/{vin}/{id}")
     * @Rest\View(serializerGroups={"customer"})
     *
     * @Rest\QueryParam(name="closed",          description="ID Customer_car")
     * @Rest\QueryParam(name="branch",          description="Код ДЦ из сервиса")
     * @Rest\QueryParam(name="date_create",     description="Дата создания ЗН")
     * @Rest\QueryParam(name="time_create",     description="Время создания ЗН")
     * @Rest\QueryParam(name="date_completion", description="Дата завершения ЗН")
     * @Rest\QueryParam(name="time_completion", description="Время завершения ЗН")
     * @Rest\QueryParam(name="title",           description="Название работ")
     * @Rest\QueryParam(name="car_run",         description="Пробег авто")
     * @Rest\QueryParam(name="status_code",     description="Статус заказ наряда")
     * @Rest\QueryParam(name="type_code",       description="Тип ЗН")
     * @Rest\QueryParam(name="works",           description="Список работ")
     * @Rest\QueryParam(name="parts",           description="Список запчастей")
     * @Rest\QueryParam(name="consultant",      description="Консультант")
     * @Rest\QueryParam(name="recommendations", description="Рекомендации")
     *
     * @param string $phone
     * @param string $vin
     * @param string $id
     * @param ParamFetcher $paramFetcher
     *
     * @return array
     */
    public function updateAction (string $phone, string $vin, string $id, ParamFetcher $paramFetcher)
    {
        $this->factoryShell = $this->get("tradedealer.factory_shell");

        /** @var UserApi $userApi */
        $userApi = $this->getUser();

        $request = array_merge([
            'phone' => $phone,
            'vin'   => $vin,
            'id'    => $id
        ], $paramFetcher->all());

        $result = $this->factoryShell->handling($userApi, $request, 'getCustomerOrderUpdate');

        return $result;
    }

    /**
     * @Rest\Delete("/{phone}/{vin}/{id}")
     * @Rest\View(serializerGroups={"external"})
     *
     *
     * @return array
     *
     */
    public function deleteAction (string $phone, string $vin, string $id)
    {
        $this->factoryShell = $this->get("tradedealer.factory_shell");

        /** @var UserApi $userApi */
        $userApi = $this->getUser();

        $request = [];

        $result = $this->factoryShell->handling($userApi, $request, 'getCustomerOrderDelete');

        return $result;
    }

}
