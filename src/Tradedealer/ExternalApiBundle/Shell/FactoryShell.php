<?php

namespace Tradedealer\ExternalApiBundle\Shell;

use Symfony\Component\DependencyInjection\Container;
use Tradedealer\EntityBundle\Entity\UserApi;
use Tradedealer\ExternalApiBundle\Exception\NotFoundEntityException;
use Tradedealer\ExternalApiBundle\Exception\NotFoundParameterInOptionException;
use Tradedealer\ExternalApiBundle\Exception\NotOptionForUserApiException;
use Tradedealer\ExternalApiBundle\Handler\AbstractHandler\AbstractHandler;
use Tradedealer\NotificationBundle\NotificationBundle;
use Tradedealer\NotificationBundle\Services\NotificationService;

class FactoryShell
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var NotificationService
     */
    private $notificationBundle;

    /**
     * FactoryShell constructor.
     * @param Container $container
     * @param NotificationService $notificationBundle
     */
    public function __construct(Container $container, NotificationService $notificationBundle)
    {
        $this->container          = $container;
        $this->notificationBundle = $notificationBundle;
    }

    /**
     * @param UserApi $userApi
     * @param array $request
     * @param string $method
     *
     * @return array
     */
    public function handling (UserApi $userApi, array $request, string $method)
    {
        $errorMessage = "Ошибка при обработке ЗН. Обратитесь к разработчикам. Текст ошибки: ";

        try {

            /** @var AbstractHandler $handler */
            $handler = $this->container
                ->get('tradedealer.factory_handler')
                ->$method($userApi);

            $result = $handler->handling($request);

            return [
                'code' => $result
            ];

        } catch (NotFoundEntityException $exception) {

            $errorMessage .= "{$exception->getMessage()}";

            $this->notificationBundle->setNotificationShortAnnotation(
                'external-api',
                [
                    'userApi'   => $userApi->getLogin(),
                    'arguments' => $request,
                    'message'   => $errorMessage . "\n" . $exception->getTraceAsString()
                ],
                new \DateTime()
            );

            return [
                'code' => $exception->getCode(),
                'message' => $errorMessage
            ];

        } catch (NotFoundParameterInOptionException $exception) {

            $errorMessage .= "{$exception->getMessage()}";

            $this->notificationBundle->setNotificationShortAnnotation(
                'external-api',
                [
                    'userApi'   => $userApi->getLogin(),
                    'arguments' => $request,
                    'message'   => $errorMessage . "\n" . $exception->getTraceAsString()
                ],
                new \DateTime()
            );

            return [
                'code' => $exception->getCode(),
                'message' => $errorMessage
            ];

        } catch (NotOptionForUserApiException $exception) {

            $errorMessage = "Ошибка при обработке ЗН. Обратитесь к разработчикам.";

            $this->notificationBundle->setNotificationShortAnnotation(
                'external-api',
                [
                    'userApi'   => $userApi->getLogin(),
                    'arguments' => $request,
                    'message'   => $exception->getMessage()
                ],
                new \DateTime()
            );

            return [
                'code' => $exception->getCode(),
                'message' => $errorMessage
            ];

        } catch (\Throwable $exception) {

            $errorMessage .= "{$exception->getMessage()}";

            $this->notificationBundle->setNotificationShortAnnotation(
                'external-api',
                [
                    'userApi'   => $userApi->getLogin(),
                    'arguments' => $request,
                    'message'   => $errorMessage . "\n" . $exception->getTraceAsString()
                ],
                new \DateTime()
            );

            return [
                'code' => $exception->getCode(),
                'message' => $errorMessage
            ];
        }
    }
}