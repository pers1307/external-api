<?php

namespace Tradedealer\ExternalApiBundle\Utils;

class Utils
{
    /**
     * @param string $string
     *
     * @return mixed
     */
    public function stringToArray(string $string)
    {
        if ($string) {
            $array = json_decode($string);
            $currentArray = current($array);

            if ($currentArray instanceof \stdClass) {
                foreach ($array as $key => $item) {
                    $array[$key] = (array) $item;
                }

                return (array) $array;
            }

            return (array) $array;
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return bool|\DateTime
     */
    public function getDateCreate(array &$data)
    {
        if ($data['date_create']) {
            $dateString = $data['date_create'];
            unset($data['date_create']);

            if ($data['time_create']) {
                $dateString .= " " . $data['time_create'];
                unset($data['time_create']);
            }

            return new \DateTime($dateString);
        }

        return false;
    }

    /**
     * @param array $data
     *
     * @return bool|\DateTime
     */
    public function getDateCompletion(array &$data)
    {
        if ($data['date_completion']) {
            $dateString = $data['date_completion'];
            unset($data['date_completion']);


            if ($data['time_completion']) {
                $dateString .= " " . $data['time_completion'];
                unset($data['time_completion']);
            }

            return new \DateTime($dateString);
        }

        return false;
    }
}