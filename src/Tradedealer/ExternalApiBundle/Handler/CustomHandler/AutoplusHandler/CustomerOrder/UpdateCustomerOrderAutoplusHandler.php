<?php

namespace Tradedealer\ExternalApiBundle\Handler\CustomHandler\AutoplusHandler\CustomerOrder;

use Tradedealer\EntityBundle\Entity\Company;
use Tradedealer\EntityBundle\Entity\CustomerCar;
use Tradedealer\EntityBundle\Entity\CustomerOrder;
use Tradedealer\EntityBundle\Entity\CustomerOrderRecommendation;
use Tradedealer\EntityBundle\Entity\CustomerOrderWork;
use Tradedealer\ExternalApiBundle\Exception\NotFoundEntityException;
use Tradedealer\ExternalApiBundle\Exception\NotFoundParameterInOptionException;
use Tradedealer\ExternalApiBundle\Handler\AbstractHandler\AbstractHandler;

class UpdateCustomerOrderAutoplusHandler extends AbstractHandler
{
    /**
     * Обязательные параметры в запросе для обновления ЗН
     *
     * @var array
     */
    private $requiredParametersForUpdate = [
        'phone',
        'vin',
        'id'
    ];

    /**
     * Обязательные параметры в запросе для добавления нового ЗН
     *
     * @var array
     */
    private $requiredParametersForNew = [
        'phone',
        'vin',
        'id',
        'closed',
        'branch',
        'closed',
        'date_create',
        'date_completion',
        'title',
        'car_run',
        'status_code',
        'works',
    ];

    /**
     * @var int
     */
    private $company = 40;

    /**
     * @param $data
     *
     * @return mixed
     */
    public function validate($data) : array
    {
        foreach ($this->requiredParametersForUpdate as $parameter) {
            if (!isset($data[$parameter])) {
                throw new NotFoundParameterInOptionException("Не найден обязательный параметр для обновления ЗН: {$parameter}");
            }
        }

        return $data;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function transform($data) : array
    {
        //Преобразованные данные
        $newData = [];

        foreach ($data as $key => $item) {

            switch ($key) {

                case "date_create":
                    $newData["date_create"] = $this->utils->getDateCreate($data);
                    break;

                case "date_completion":
                    $newData["date_completion"] = $this->utils->getDateCompletion($data);
                    break;

                case (
                    $key == "works"      ||
                    $key == "parts"      ||
                    $key == "consultant" ||
                    $key == "recommendations"
                ):
                    $newData[$key] = is_string($item) ? $this->utils->stringToArray($item) : $item;
                    break;

                default:
                    $newData[$key] = $item;
                    break;
            }

        }

        return $newData;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function processing($data) : int
    {
        $customerOrder = $this->entityManager->getRepository("EntityBundle:CustomerOrder")
            ->findOneBy([
                'externalId' => $data['id'],
            ]);

        if (is_null($customerOrder)) {
            $customerOrder = new CustomerOrder();
            $this->validateDataForNewCustomerOrder($data);
        }

        $customerOrder = $this->insertOrUpdateCustomerOrder($customerOrder, $data);

        if (isset($data['works'])) {
            $this->insertOrUpdateWorks($customerOrder, $data['works']);
        }

//            if (isset($data['recommendations'])) {
//                $this->insertOrUpdateRecommendations($customerOrder, $data['recommendations']);
//            }

        return 200;
    }

    /**
     * Вставляет новый или обновляет уже имеющийся ЗН
     *
     * @param CustomerOrder $customerOrder
     * @param array         $data
     *
     * @return CustomerOrder
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function insertOrUpdateCustomerOrder (CustomerOrder $customerOrder, array $data): CustomerOrder
    {
        $customerOrder->setExternalId($data['id']);
        $customerOrder->setCompany($this->getCompany());

        $customerOrder->setCustomerCar($this->getCustomerCar($data['vin']));

        if (isset($data['date_create']) && $data['date_create'] != '') {
            $customerOrder->setCreationDate($data['date_create']);
        }

        if (isset($data['date_completion']) && $data['date_completion'] != '') {
            $customerOrder->setCloseDate($data['date_completion']);
        }

        if (isset($data['car_run']) && $data['car_run'] != '') {
            $customerOrder->setCarRun($data['car_run']);
        }

        if (isset($data['status_code']) && $data['status_code'] != '') {
            $customerOrder->setStatusCode($data['status_code']);
        }

        if (isset($data['type_code']) && $data['type_code'] != "") {
            $customerOrder->setTypeCode($data['type_code']);
        }

        if (isset($data['closed']) && $data['closed'] != "") {
            $customerOrder->setClosed($data['closed']);
        }

        if (isset($data['consultant']) && $data['consultant'] != "") {
            $customerOrder->setConsultant($data['consultant']);
        }

        if (isset($data['parts']) && $data['parts'] != "") {
            $customerOrder->setParts($data['parts']);
        }

        $this->entityManager->persist($customerOrder);
        $this->entityManager->flush();

        return $customerOrder;
    }

    /**
     * @param CustomerOrder $customerOrder
     * @param array $works
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function insertOrUpdateWorks (CustomerOrder $customerOrder, array $works)
    {
        $customerOrderWorks = $customerOrder->getWorks();
        $customerOrderWorks = $customerOrderWorks->getValues();

        foreach ($works as $work) {

            $foundOrder = null;

            if (count($customerOrderWorks) > 0) {
                foreach ($customerOrderWorks as $key => $customerOrderWork) {
                    if ($customerOrderWork->getCode() == $work['code']) {
                        unset($customerOrderWorks[$key]);
                        $foundOrder = $customerOrderWork;
                        break;
                    }
                }
            }

            if (!isset($foundOrder)) {
                $customerOrderWork = new CustomerOrderWork();
            }

            $customerOrderWork->setTitle($work['title']);
            $customerOrderWork->setCode($work['code']);
            $customerOrderWork->setQuantity($work['qty']);
            $customerOrderWork->setPrice($work['price']);
            $customerOrderWork->setAmount($work['amount']);
            $customerOrderWork->setAddWork($work['add_work']);
            $customerOrderWork->setCustomerOrder($customerOrder);

            $this->entityManager->persist($customerOrderWork);
            $this->entityManager->flush();
        }

        if (count($customerOrderWorks) > 0) {
            $this->removeWorks($customerOrderWorks);
        }
    }

    /**
     * Удаляет работы, которые есть у нас, но которых не передаёт АП
     *
     * @param array $works
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function removeWorks (array $works)
    {
        foreach ($works as $work) {
            $this->entityManager->remove($work);
        }

        $this->entityManager->flush();
    }


    /**
     * @deprecated
     *
     * Непонятно, что делать с этим методом
     * История с рекоммендациями неясна
     * todo: Использовать и доделать, когда станут ясны подробности
     *
     *
     *
     * @param CustomerOrder $customerOrder
     * @param array $recommendations
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function insertOrUpdateRecommendations (CustomerOrder $customerOrder, array $recommendations)
    {
        $customerOrderRecommendations = $customerOrder->getRecommendations();
        $customerOrderRecommendations = $customerOrderRecommendations->getValues();

        foreach ($recommendations as $recommendation) {

            if (count($customerOrderRecommendations) > 0) {
                foreach ($customerOrderRecommendations as $key => $customerOrderRecommendation) {
                    if ($customerOrderRecommendation->getCode() == $recommendation['code']) {
                        break;
                    }
                }
            }

            if (!isset($customerOrderRecommendation)) {
                $customerOrderRecommendation = new CustomerOrderRecommendation();
            }

            $customerOrderRecommendation->setCode($recommendation['code']);
            $customerOrderRecommendation->setCustomerOrder($customerOrder);
            $customerOrderRecommendation->setTitle($recommendation['description']);
            /**
             * todo: дописать недостоющие данные
             */

            $this->entityManager->persist($customerOrderRecommendation);
            $this->entityManager->flush();
        }
    }


    /**
     * @return Company
     */
    private function getCompany () : Company
    {
        return $this->entityManager->getRepository("EntityBundle:Company")
            ->find($this->company);
    }

    /**
     * @param string $vin
     * @return CustomerCar
     */
    private function getCustomerCar (string $vin) : CustomerCar
    {
        $car = $this->entityManager->getRepository("EntityBundle:Car")
            ->findOneBy([
                'vin' => $vin
            ]);

        if (is_null($car)) {
            throw new NotFoundEntityException("Не найдена сущность Car");
        }

        $customerCar = $car->getCustomerCars();
        $customerCar = $customerCar->first();

        if (is_null($customerCar)) {
            throw new NotFoundEntityException("Не найдена сущность CustomerCar");
        }

        return $customerCar;
    }

    /**
     * Валидация данных, если происходит добавление нового ЗН
     *
     * @param array $data
     */
    private function validateDataForNewCustomerOrder (array $data)
    {
        foreach ($this->requiredParametersForNew as $parameter) {
            if (!isset($data[$parameter])) {
                throw new NotFoundParameterInOptionException("Не найден обязательный параметр для добавления нового ЗН: {$parameter}");
            }
        }
    }


}