<?php

namespace Tradedealer\ExternalApiBundle\Handler\CustomHandler\AutoplusHandler\CustomerOrder;

use Tradedealer\ExternalApiBundle\Handler\AbstractHandler\AbstractHandler;

class UpdateCustomerOrderBaseHandler extends AbstractHandler
{
    public function validate($data)
    {

    }

    public function transform($data)
    {

    }

    public function processing($data)
    {

    }
}