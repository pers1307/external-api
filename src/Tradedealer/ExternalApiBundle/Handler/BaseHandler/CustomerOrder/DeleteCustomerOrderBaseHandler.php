<?php

namespace Tradedealer\ExternalApiBundle\Handler\BaseHandler\CustomerOrder;

use Tradedealer\ExternalApiBundle\Handler\AbstractHandler\AbstractHandler;

class DeleteCustomerOrderBaseHandler extends AbstractHandler
{
    public function validate($data)
    {

    }

    public function transform($data)
    {

    }

    public function processing($data)
    {

    }
}