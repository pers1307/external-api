<?php

namespace Tradedealer\ExternalApiBundle\Handler\AbstractHandler;

use Doctrine\ORM\EntityManager;
use Tradedealer\ExternalApiBundle\Interfaces\Handler\HandlingInterface;
use Tradedealer\ExternalApiBundle\Interfaces\Handler\ProcessingInterface;
use Tradedealer\ExternalApiBundle\Interfaces\Handler\TransformInterface;
use Tradedealer\ExternalApiBundle\Interfaces\Handler\ValidateInterface;
use Tradedealer\ExternalApiBundle\Shell\FactoryShell;
use Tradedealer\ExternalApiBundle\Utils\Utils;

abstract class AbstractHandler implements ProcessingInterface, TransformInterface, ValidateInterface, HandlingInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Utils
     */
    protected $utils;

    /**
     * AbstractHandler constructor.
     *
     * @param EntityManager $entityManager
     * @param Utils $utils
     */
    public function __construct(
        EntityManager $entityManager,
        Utils $utils
    ) {
        $this->entityManager = $entityManager;
        $this->utils = $utils;
    }


    public function handling($data)
    {
        $data = $this->transform($data);

        $this->validate($data);

        return $this->processing($data);
    }

    public abstract function validate($data);

    public abstract function transform($data);

    public abstract function processing($data);
}