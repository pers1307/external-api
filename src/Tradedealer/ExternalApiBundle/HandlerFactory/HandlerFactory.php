<?php

namespace Tradedealer\ExternalApiBundle\HandlerFactory;

use Doctrine\ORM\EntityManager;
use Tradedealer\EntityBundle\Entity\UserApi;
use Tradedealer\ExternalApiBundle\Exception\NotFoundParameterInOptionException;
use Tradedealer\ExternalApiBundle\Exception\NotOptionForUserApiException;
use Tradedealer\ExternalApiBundle\Interfaces\Factory\CustomerOrderInterface;
use Tradedealer\ExternalApiBundle\Interfaces\Handler\HandlingInterface;
use Tradedealer\ExternalApiBundle\Shell\FactoryShell;
use Tradedealer\ExternalApiBundle\Utils\Utils;

class HandlerFactory implements CustomerOrderInterface
{
    protected $handlerMapping = [];

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Utils
     */
    protected $utils;

    /**
     * HandlerFactory constructor.
     *
     * @param EntityManager $entityManager
     * @param Utils $utils
     * @param $options
     */
    public function __construct(
        EntityManager $entityManager,
        Utils $utils,
        $options
    ) {
        foreach ($options as $option) {
            $this->handlerMapping[$option['userApiId']] = $option;
        }

        $this->entityManager = $entityManager;
        $this->utils = $utils;
    }

    /**
     * @param UserApi $userApi
     *
     * @return HandlingInterface
     */
    public function getCustomerOrderInsert(UserApi $userApi)
    {
        $option = $this->searchOption($userApi);
        $class  = $this->getParameterFromOption('insertCustomerOrderHandler', $option);

        return new $class($this->entityManager, $this->utils);
    }

    /**
     * @param UserApi $userApi
     *
     * @return HandlingInterface
     */
    public function getCustomerOrderUpdate(UserApi $userApi)
    {
        $option = $this->searchOption($userApi);
        $class  = $this->getParameterFromOption('updateCustomerOrderHandler', $option);

        return new $class($this->entityManager, $this->utils);
    }

    /**
     * @param UserApi $userApi
     *
     * @return HandlingInterface
     */
    public function getCustomerOrderDelete(UserApi $userApi)
    {
        $option = $this->searchOption($userApi);
        $class  = $this->getParameterFromOption('deleteCustomerOrderHandler', $option);

        return new $class($this->entityManager, $this->utils);
    }

    /**
     * @param UserApi $userApi
     *
     * @return array
     */
    public function searchOption(UserApi $userApi)
    {
        if (isset($this->handlerMapping[$userApi->getId()])) {
            return $this->handlerMapping[$userApi->getId()];
        }

        throw new NotOptionForUserApiException('Не найдены настройки для userApi c id:' . $userApi->getId());
    }

    /**
     * @param string $parameterName
     * @param array  $option
     *
     * @return string
     */
    public function getParameterFromOption($parameterName, $option)
    {
        if (isset($option[$parameterName])) {
            return $option[$parameterName];
        }

        throw new NotFoundParameterInOptionException(
            'В опциях для userApi c id: ' . $option['userApiId'] . ' не найден параметр ' . $parameterName
        );
    }
}